#!/bin/bash

## VARIABLES ##

nom="MyWeb"																# Nombre con que identificas este respaldo
origen="CarpetaOrigen"													# Carpeta a salvar de tu host

# Datos conexión SSH #
sshUser="UserSsh"														# Usuario para la conexión ssh
sshPass="PassSsh"														# Contraseña ssh de tu host
sshHost="HostSsh"														# Dirección conexión ssh

# Datos conexión DATABASE #
dbUser="UserDB"															# Usuario conexión db
dbPass="PassDB"															# Contraseña conexión db
dbHost="HostDB"															# Dirección conexión db
dbName="NombreDB"														# Nombre de la base de datos

# Directorios #
dest="$HOME/Backups/$nom/Temporales/archivos"							## RUTAS RECOMENDADAS # Carpeta destino de las copias temporales
destMes="$HOME/Backups/$nom/Mensuales"									## RUTAS RECOMENDADAS # Carpeta destino de las copias mensuales
destDb="$HOME/Backups/$nom/Temporales/DB"								## RUTAS RECOMENDADAS # Carpeta destino de las copias de DB
logs="$HOME/Backups/$nom/Temporales/logs"								## RUTAS RECOMENDADAS # Carpeta destino de logs de copias

rutas=(" $dest $logs $destDb $destMes " )     							## NO TOCAR!! # Para crear los directorios
deps=(" openssh mariadb expect tar rsync curl" )						## NO TOCAR!! # Dependencias necesarias para que funcione el script
deps_deb=(" openssh-client mariadb-client expect tar rsync curl" )		## NO TOCAR!! # Dependencias necesarias para que funcione el script

fecha="$(date '+%d-%m-%Y_%H:%M')"  										## NO TOCAR!! # Decha y hora actual para los logs
diaSem="$(date '+%u')"													## NO TOCAR!! # Día de la semana numérico Lunes=1, martes=2, etc...
diaSemA="$(date '+%A')"													## NO TOCAR!! # Día de la semana, Nombre completo
diaNum="$(date '+%d')"													## NO TOCAR!! # Día del mes
diaMes="$(date '+%d_%B')"												## NO TOCAR!! # Día y mes
lun="$(date -dlast-monday +%A)"											## NO TOCAR!! # Lunes en el idioma del sistema, para que funcione en todos los equipos.


# CREA LA CLAVE SSH SI NO ESTÁ CREADA EN NUESTRO EQUIPO
if [ ! -f $HOME/.ssh/idrsa-1 ]; then
  ssh-keygen -t rsa -N "$sshPass" -f $HOME/.ssh/idrsa-1
	expect << EOF
		spawn ssh-add $HOME/.ssh/idrsa-1
		expect "Enter passphrase for $HOME/.ssh/idrsa-1:"
		send "$sshPass\r"
		expect eof
EOF
fi

# INTRODUCIMOS EN EL EQUIPO REMOTO LA CLAVE SSH SI NO EXISTE
expect -c "
  log_user 0
	spawn ssh-copy-id -i $HOME/.ssh/idrsa-1.pub -o StrictHostKeyChecking=no ${sshUser}@${sshHost}
  match_max 100000
	expect \"*?assword:*\" { send -- \"$sshPass\r\"}
	sleep 1
	log_user 1
	exit"

# COMPROBAMOS SI TENEMOS LOS DIRECTORIOS PARA LAS COPIAS, DE LO CONTRARIO LOS CREA
for f in $rutas; do
	[ -d $rutas 2> /dev/null ] && direc+="$f " || ndirec+="$f "
done
mkdir -p $ndirec

# COMPROBAMOS E INSTALAMOS LOS PAQUETES NECESARIOS PARA DISTRIBUCIONES ARCHLINUX Y DEBIAN
install_deps () {
	if [ -x "$(command -v apt-get)" ]; then
		for e in ${deps_deb[*]}
		do
			if ! (which $e >/dev/null); then
				insta_deb=$insta_deb" "$e
			fi
		done
		if [ -n "$insta_deb" ]; then
			echo "Necesitamos instalar: $insta_deb"
			sleep 3
			sudo apt install -y $insta_deb
		fi
	elif [ -x "$(command -v pacman)" ]; then
		for d in ${deps[*]}
		do
			if ! (pacman -Q $d >/dev/null); then
				insta_arch=$insta_arch" "$d
			fi
		done
		if [ -n "$insta_arch" ]; then
			echo "Necesitamos instalar: $insta_arch"
			sleep 3
			sudo pacman -S $insta_arch --noconfirm
		fi
	else
		echo "Este gestor de paquetes no es compatible con el script. Instala las dependencias manualmente." >&2;
		exit 1;
	fi
}

install_deps

# Copia mensual completa - se realiza cada día 1
if [ $diaNum = "01" ]; then
	rsync -avzz --delete --timeout=20 -e "ssh" $sshUser@$sshHost:$origen/ $destMes/$diaMes > $logs/back_$fecha.log 2>&1
	ssh $sshUser@$sshHost "mysqldump -h $dbHost -u $dbUser -p$dbPass -B $dbName | gzip -9 -c" > $destMes/$diaMes/DB_$diaMes.sql.gz
	tar -cjf $destMes/$nom_$diaMes.tar.bz2 $destMes/$diaMes
	rm -Rf $destMes/$diaMes
fi

# Copia semanal completa - se realiza cada domingo
if [ $diaSem = "1" ]; then
	rsync -avzz --delete --timeout=20 -e "ssh" $sshUser@$sshHost:$origen/ $dest/$diaSemA > $logs/back_$fecha.log 2>&1
else
	rsync -avzz --delete --timeout=20 --compare-dest=$dest/$lun -e "ssh" $sshUser@$sshHost:$origen/ $dest/$diaSemA > $logs/back_$fecha.log 2>&1
fi

# RESPALDO DE LA BASE DE DATOS - Todos los días menos el 1ero de cada mes
if [ ! $diaNum = "01" ]; then
	ssh $sshUser@$sshHost "mysqldump -h $dbHost -u $dbUser -p$dbPass -B $dbName | gzip -9 -c" > $destDb/DB_$diaMes.sql.gz
fi

# BORRAD0 DE ARCHIVOS ANTIGUOS - Se eliminan los logs y bases de datos de más de un mes de antiguedad.
find $logs/* -type f -mtime +30 -exec rm -f \{\} \;
find $destDb/* -type f -mtime +30 -exec rm -f \{\} \;
